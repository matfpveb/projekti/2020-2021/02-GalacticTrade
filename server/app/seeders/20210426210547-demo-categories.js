'use strict';

const db=require('../models');
const Group=db.Group;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const components=await Group.findOne({where:{name:"Racunarske komponente"}});
    const configs=await Group.findOne({where:{name:"Desktop konfiguracije"}});
    const laptops=await Group.findOne({where:{name:"Laptop racunari"}});
    return await queryInterface.bulkInsert("Categories",[
      {
        name:"Graficke kartice",
        GroupId:components.id
      },
      {
        name:"Procesori",
        GroupId:components.id
      },
      {
        name:"Maticne ploce",
        GroupId:components.id
      },


      {
        name:"Gejmerske konfiguracije",
        GroupId:configs.id
      },
      {
        name:"Gejmerske polukonfiguracije",
        GroupId:configs.id
      },

      {
        name:"Gejmerski laptop racunari",
        GroupId:laptops.id
      },
      {
        name:"Ostali laptop racunari",
        GroupId:laptops.id
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('Categories',{});
  }
};
