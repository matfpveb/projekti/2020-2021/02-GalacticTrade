'use strict';
const {
  Model
} = require('sequelize');
// const Group=require('./index').Group;
module.exports = (sequelize, DataTypes) => {
  class Category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Category.belongsTo(models.Group)
      models.Category.hasMany(models.Product)
    }
  };
  Category.init({
    name: {
      type:DataTypes.STRING,
      validate:{
        notEmpty:true
      }
    },
    description: DataTypes.TEXT,
    GroupId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Category',
    timestamps:false
  });
  return Category;
};