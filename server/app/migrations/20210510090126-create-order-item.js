'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('OrderItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      count: {
        type: Sequelize.INTEGER,
        default:1
      },
      OrderId: {
        type: Sequelize.INTEGER,
        references: {
          model:'Orders',
          key:'id'
        }
      },
      ProductId: {
        type: Sequelize.INTEGER,
        references: {
          model:'Products',
          key:'id'
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('OrderItems');
  }
};