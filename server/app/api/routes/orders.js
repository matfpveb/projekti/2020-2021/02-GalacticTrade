const express = require('express');
const router = express.Router();
const controller = require('../controllers/orderController');
const authenticated = require('../middleware/authenticated');

router.get('/get/id/:id', controller.getById);
router.get('/get/all', controller.getAll);
router.get('/get/live',controller.liveSearch);
router.get('/get/total',controller.totalOrderValue);
router.get('/get/top',controller.topSelling);

router.post('/create', controller.addOrder);
router.delete('/delete/:id', controller.deleteOrder);

module.exports = router;
