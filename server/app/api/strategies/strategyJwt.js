const passport=require('passport');
const { ExtractJwt,Strategy }=require('passport-jwt');
// import db from '../../models'
// const User=db.User;
const { User }=require('../../models');
passport.use(
    new Strategy({
        jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey:process.env.COOKIE_SECRET
    },async (jwtPayload,done)=>{
        try {
            const user=await User.findByPk(jwtPayload.id);
            if(!user)
                return done(null,false);
            return done(null,user);
        }catch (e)
        {
            return done(e);
        }
    })
);
