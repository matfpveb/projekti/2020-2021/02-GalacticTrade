const db = require('../../models');
const Product = db.Product;
const OrderItem=db.OrderItem;
const {ValidationError,Op,cast,where}=require('sequelize')
const sequelize=require('sequelize')
const path=require('path');

async function getById(req, res, next) {
    const id = req.params.id; 
    if(!id)
        return res.status(400).send({"message":"No product id provided"});
    try {
        const result = await Product.findByPk(id);
        if(!result) {
            return res.status(404).send({"message": "No such product in the database"});
        }
        return res.status(200).json(result);
    } catch (e) {
        e.status = 500;
        next(e);
    }
};

async function addNewProduct(req, res, next) {
    const product = {
        name: req.body.name, 
        description: req.body.description,
        price: req.body.price,
        discount: req.body.discount, 
        CategoryId: req.body.CategoryId,
    };
    const imgFormats=[".jpg",".jpeg",".png",".gif"];
    if(req.file!==undefined && imgFormats.includes(path.extname(req.file.filename)))
        product.img=req.file.filename;

    if(!product.name || !product.price || !product.CategoryId) {
        return res.status(400).send({"message":"Missing product information"});
    }
    try {
        const newProduct = await Product.create(product);
        return res.status(200).json({"message" : "Product added successfully"});
    } catch(e) {
        e.status = 400;
        next(e);
    }
};

async function deleteProduct(req, res, next) {
    const id = req.params.id;
    if(!id)
        return res.status(400).send({"message": "No product id provided"});
    try {
        const product = await Product.findByPk(id);
        if(!product) {
            return res.status(404).send({"message": "No such product in the database"});
        }
        product.destroy();
        return res.status(200).json({"message" : "Product deleted successfully"});
    } catch (e) {
        e.status = 500;
        next(e);
    }
};

async function editProduct(req, res, next) {
    const {name, description,price, discount} = req.body;
    const id=req.params.id;
    const imgFormats=[".jpg",".jpeg",".png",".gif"];
    let img=undefined
    if(req.file!==undefined && imgFormats.includes(path.extname(req.file.filename)))
        img = req.file.filename;
    try {
        const product = await Product.findByPk(id);
        if(!product) {
            return res.status(404).send({"message": "No such product in the database"});
        }
        if(name) {
            product.setDataValue('price', price);
        }
        if(description) {
            product.setDataValue('description', description);
        }
        if(img) {
            product.setDataValue('img', img);
        }
        if(price) {
            product.setDataValue('price', price);
        }
        if(discount) {
            product.setDataValue('discount', discount);
        }
        await product.save();
        return res.status(200).json({"message" : "Product edited successfully"});
    } catch(e) {
        e.status = 500;
        next(e);
    };
};

async function liveSearch(req,res,next){
    const CategoryId = req.query.CategoryId;
    const minPrice = req.query.minPrice;
    const maxPrice = req.query.maxPrice;
    const withDiscount = req.query.withDiscount;

    let search=req.query.search ?? ""
    try {
        let searchProperty={[Op.iLike]:'%'+search+'%'}
        let queryObject={
            [Op.or]:[
                {name:searchProperty},
                sequelize.where(
                    sequelize.cast(sequelize.col('Product.id'),'varchar'),
                    searchProperty
                )
            ]
        };
        if(CategoryId)
            queryObject.CategoryId=CategoryId;
        if(minPrice || maxPrice)
            queryObject.price={};
        if(minPrice){
            queryObject.price[Op.gte] = minPrice;
        }
        if(maxPrice){
            queryObject.price[Op.lte] = maxPrice;
        }
        if(withDiscount){
            queryObject.discount = {};
            queryObject.discount[Op.gt] = 0;
        }

        let orderArray=[];
        const sortName=req.query.sortName;
        const sortPrice=req.query.sortPrice;
        if(sortName)
            orderArray.push(['name',sortName]);
        if(sortPrice)
            orderArray.push(['price',sortPrice]);

        const results=await Product.findAll({
            where:queryObject,
            order:orderArray
        });

        let orderItems=await OrderItem.findAll();
        orderItems=orderItems.map(item => item.dataValues.ProductId);
        const set=new Set(orderItems);


        for await ( const iterator of results) {
            iterator.dataValues.count = 0;
            iterator.dataValues.deletable=!set.has(iterator.dataValues.id);
        }
        return res.status(200).json(results);
    }catch (e)
    {
        e.status=500
        next(e)
    }
}

async function priceRange(req,res,next){
    const minPrice = req.query.minPrice;
    const maxPrice = req.query.maxPrice;
    const CategoryId = req.query.CategoryId;

    try {

        let queryObj={};
        if(CategoryId){
            queryObj.CategoryId = CategoryId;
        }
        if(minPrice || maxPrice)
            queryObj.price={};
        if(minPrice){
            queryObj.price[Op.gte] = minPrice;
        }
        if(maxPrice){
            queryObj.price[Op.lte] = maxPrice;
        }
        const results=await Product.findAll({where:queryObj});
        return res.status(200).json(results);
    }catch (e)
    {
        e.status=500
        next(e)
    }
}

async function findCategory(req,res,next){
    const id = req.params.id;
    if(!id){
        res.status(400).json({"message" : "No category with this id!"});
    }
    try {
        const product = await Product.findByPk(id);
        if(!product) {
            return res.status(404).send({"message": "No such product in the database"});
        }
        const category = await product.getCategory();
        res.status(200).json(category);
    }catch (e)
    {
        e.status=500
        next(e)
    }
}

async function findGroup(req,res,next){
    const id = req.params.id;
    if(!id){
        res.status(400).json({"message" : "No category with this id!"});
    }
    try {
        const product = await Product.findByPk(id);
        if(!product) {
            return res.status(404).send({"message": "No such product in the database"});
        }
        const group = await product.getGroup();
        res.status(200).json(group);
    }catch (e)
    {
        e.status=500
        next(e)
    }
}

module.exports = {
    getById,
    addNewProduct,
    deleteProduct,
    editProduct,
    liveSearch,
    priceRange,
    findCategory,
    findGroup,
};
