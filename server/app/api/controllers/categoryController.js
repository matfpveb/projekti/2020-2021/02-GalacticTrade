const db = require('../../models');
const Category=db.Category;
const {Op}=require('sequelize')
const sequelize=require('sequelize')

async function getById(req, res, next) {
    const id = req.params.id;
    if(!id)
        return res.status(400).send({"message":"No category id provided"});
    try {
        const result = await Category.findByPk(id);
        if(!result) {
            return res.status(404).send({"message": "No such category"});
        }
        return res.status(200).json(result);
    } catch (e) {
        next(e);
    }
}

async function getProducts(req,res,next) {
    const id=req.params.id;
    if(!id)
        return res.status(400).send({"message":"No category id provided"});
    try{
        const category=await Category.findByPk(id);
        if(!category)
            return res.status(404).send({"message": "No such category"});
        const products=await category.getProducts();
        return res.status(200).json(products);
    }catch (e) {
        next(e);
    }
}

async function getGroup(req,res,next) {
    const id=req.params.id;
    if(!id)
        return res.status(400).send({"message":"No category id provided"});
    try{
        const category=await Category.findByPk(id);
        if(!category)
            return res.status(404).send({"message": "No such category"});
        const group=await category.getGroup();
        return res.status(200).json(group);
    }catch (e) {
        next(e);
    }
}

async function liveSearch(req,res,next) {
    let search=req.query.search ?? "";
    try {
        let searchProperty={[Op.iLike]:'%'+search+'%'};
        let results=await Category.findAll({
            where:{
                [Op.or]:[
                    {name:searchProperty},
                    {description:searchProperty},
                    sequelize.where(
                        sequelize.cast(sequelize.col('Category.id'),'varchar'),
                        searchProperty
                    )
                ]
            }
        });
        return res.status(200).json(results);
    }catch (e)
    {
        next(e);
    }
}

async function create(req, res, next) {
    const category = {
        name: req.body.name,
        description: req.body.description,
        GroupId: req.body.GroupId
    };
    if(!category.name || !category.GroupId) {
        return res.status(400).send({"message":"Missing category information"});
    }
    try {
        const newCategory = await Category.create(category);
        return res.status(200).json({"message" : "Category added successfully"});
    } catch(e) {
        next(e);
    }
}

async function destroy(req, res, next) {
    const id = req.params.id;
    if(!id)
        return res.status(400).send({"message": "No category id provided"});
    try {
        const category = await Category.findByPk(id);
        if(!category) {
            return res.status(404).send({"message": "No such category"});
        }
        if(await category.countProducts()!==0)
            return res.status(400).send({"message":"Can't delete a category if it has any products"});
        await category.destroy();
        return res.status(200).json({"message" : "success"});
    } catch (e) {
        next(e);
    }
}

async function edit(req, res, next) {
    const id=req.params.id;
    const {name, description,GroupId} = req.body;
    try {
        const category = await Category.findByPk(id);
        if(!category) {
            return res.status(404).send({"message": "No such category"});
        }
        if(name) {
            category.setDataValue('name', name);
        }
        if(description) {
            category.setDataValue('description', description);
        }
        if(GroupId) {
            category.setDataValue('GroupId', GroupId);
        }
        await category.save();
        return res.status(200).json({"message" : "Category edited successfully"});
    } catch(e) {
        next(e);
    }
}

module.exports = {getById,getProducts,getGroup,liveSearch,create,edit,destroy}
