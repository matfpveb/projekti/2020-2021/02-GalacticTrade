const db=require('../../models')
const User=db.User
const bcrypt=require('bcrypt')
const {ValidationError,Op,cast,where}=require('sequelize')
const sequelize=require('sequelize')
const jwt=require('jsonwebtoken');
const {ExtractJwt}=require('passport-jwt');

async function register(req,res,next) {
    try {
        fields = {
            username:req.body.username,
            fullname:req.body.fullname,
            password:await bcrypt.hash(req.body.password,10),
            email:req.body.email,
            isAdmin:req.body.isAdmin
        }
        newUser=await User.create(fields)
        return res.status(201).json({id:newUser.id,username:newUser.username,message:"New user registered"})
    }catch (e) {
        if(e instanceof ValidationError){
            e.status=400
        }
        else
            e.status=500
        next(e)
    }
}

async function getAll(req,res,next){

    try {
        let results=await User.findAll()
        return res.status(200).send(results)
    }catch (e){
        e.status=500
        next(e)
    }
}
async function getById(req,res,next){
    let id
    if(req.params.id)
        id=req.params.id ?? req.body.id
    if(id===undefined)
        return res.status(400).send({"message":"No id provided"})
    try {
        let result=await User.findByPk(id)
        return res.status(200).send(result)
    }
    catch (e)
    {
        e.status=500
        next(e)
    }
}
async function liveSearch(req,res,next){
    console.log(req.query.search)
    let search=req.query.search ?? ""
    try {
        let searchProperty={[Op.iLike]:'%'+search+'%'}
        let results=await User.findAll({
            where:{
                [Op.or]:[
                    {username:searchProperty},
                    {fullname:searchProperty},
                    {email:searchProperty},
                    sequelize.where(
                        sequelize.cast(sequelize.col('User.id'),'varchar'),
                        searchProperty
                    )
                ]
            }
        })
        return res.status(200).send(results)
    }catch (e)
    {
        e.status=500
        next(e)
    }
}
async function login(req,res,next){
    const {username,password}=req.body;
    if(username && password){
        const user=await User.findOne({where:{username:username}});
        if(!user)
            return res.status(404).json({message:"No such user"});
        if(await user.checkPassword(password))
        {
            const token=jwt.sign({id: user.id},process.env.COOKIE_SECRET,{expiresIn: '1h'});
            user.setDataValue('password',undefined);
            return res.status(200).json({message:"Login successful",token:token,user:user});
        }
        return res.status(401).json({message:"Wrong password"});
    }
}
function logout(req,res){
    req.logout()
    res.status(200).json({"message":"Logged out"});
}
async function authUser(req,res,next){
    try {
        if(!req.user)
            return res.status(404).json({"message":"Not logged in"})
        const result = Object.assign(req.user.dataValues,{password:undefined})
        return res.status(200).json(result);
    }
    catch (e)
    {
        e.status=500
        next(e)
    }
}

module.exports = {register,getAll,getById,liveSearch,login,logout,authUser}

