const db = require('../../models');
const Group = db.Group;
const Category=db.Category;

async function getById(req, res, next) {
    const id = req.params.id; 
    if(!id) {
        return res.status(400).send({"message":"No group id provided"});
    }
    try {
        const result = await Group.findByPk(id);
        if(!result) {
            return res.status(404).send({"message": "No such group in the database"});
        }
        return res.status(200).json(result);
    } catch (e) {
        e.status = 500;
        next(e);
    }
}

async function getAll(req, res, next){

    try {
        let results = await Group.findAll({include: Category});
        for (const iterator of results) {
            for (const category of iterator.dataValues.Categories) {
                category.dataValues.deletable=(await category.countProducts())===0;
            }
        }
        return res.status(200).json(results)
    } catch (e) {
        e.status = 500
        next(e)
    }
}

async function getCategories(req, res, next) {
    const id = req.params.id;
    if(!id)
        return res.status(400).send({"message":"No group id provided"});
    try{
        const group = await Group.findByPk(id);
        if(!group)
            return res.status(404).json({"message":"No such group"});
        const categories = await group.getCategories();
        return res.status(200).json(categories);
    }catch (e) {
        next(e);
    }
}

async function getProducts(req, res, next) {
    const id = req.params.id;
    if(!id)
        return res.status(400).send({"message":"No group id provided"});
    try{
        const group = await Group.findByPk(id);
        const products = await group.getProducts();
        return res.status(200).json(products);
    }catch (e) {
        next(e);
    }
}

async function createNewGroup(req, res, next) {
    const group = {
        name: req.body.name, 
        description: req.body.description,
    }
    if(!group.name) {
        return res.status(400).send({"message":"Missing group information"});
    }
    try {
        const newGroup = await Group.create(group);
        return res.status(200).json({"message" : "Group added successfully"});
    } catch(e) {
        e.status = 400;
        next(e);
    }
}

async function editGroup(req, res, next) {
    const id = req.params.id;
    const {name, description} = req.body;
    try {
        const group = await Group.findByPk(id);
        if(!group) {
            return res.status(404).send({"message": "No such group in the database"});
        }
        if(name) {
            group.setDataValue('name', name);
        }
        if(description) {
            group.setDataValue('description', description);
        }
        await group.save();
        return res.status(200).json({"message" : "Group edited successfully"});
    } catch(e) {
        next(e);
    }
}

async function deleteGroup(req, res, next) {
    const id = req.params.id;
    if(!id)
        return res.status(400).send({"message": "No group id provided"});
    try {
        const group = await Group.findByPk(id);
        if(!group) {
            return res.status(404).send({"message": "No such group in the database"});
        }
        if(await group.countCategories()!==0)
            return res.status(400).send({"message":"Can't delete a group if it has any category"});
        await group.destroy();
        return res.status(200).json({"message" : "success"});
    } catch (e) {
        next(e);
    }
}

module.exports = {
    getById,
    getAll,
    getCategories,
    getProducts,
    createNewGroup,
    editGroup,
    deleteGroup
}
