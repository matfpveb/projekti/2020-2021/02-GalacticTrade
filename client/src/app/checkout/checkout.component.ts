import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductService } from '../services/product.service';
import {first} from "rxjs/operators";
import { fadeInItems } from '@angular/material/menu';
import { Product } from '../models/product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  public firstName: string;
  public lastName: string;
  public address: string;
  public city: string;
  public email: string;
  // public deliveryDate: string;
  public paymentMethod: string;

  constructor(private _productService:ProductService, private router:Router) { }

  ngOnInit(): void {
  }

  onPaymentMethodChange(value: string): void {
    if(value) {
      this.paymentMethod = value;
    }
  }

  // onDateChange(event: string): void {
  //   this.deliveryDate = event;
  // }

  checkout(): void {
    let cartInfo: Product[] = JSON.parse(localStorage.getItem('myData'));
    if(cartInfo.length === 0) {
      window.alert("You have nothing in the cart to order!");
      return;
    }
    if(!this.firstName || !this.lastName || !this.city || !this.email || !this.address || !this.paymentMethod)  {
        window.alert("Missing order information!");
        return;
    }
    let orderData: any = {
      firstName: this.firstName,
      lastName: this.lastName,
      city: this.city,
      email: this.email,
      address: this.address,
      // deliveryDate: this.deliveryDate,
      paymentMethod: this.paymentMethod,
      items: [],
    };
    for(let item of cartInfo) {
      orderData.items.push({"ProductId":item.id, "count":item.count});
    }

    const result:Observable<any>=this._productService.createOrder(orderData);
    result.pipe(first()).subscribe(object => {
      window.alert(object.message);
    });

    this.router.navigate(["/"]);
    //TODO: Bring user back to homepage at this point in code.
  }
}
