import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {catchError, take} from "rxjs/operators";
import {of} from "rxjs";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  get authService(): AuthService {
    return this._authService;
  }

  public registerForm:FormGroup | null=null;

  constructor(private _authService: AuthService,private formBuilder:FormBuilder,public dialogRef:MatDialogRef<RegisterComponent> ) {
    this.registerForm=this.formBuilder.group({
      username:['',Validators.required],
      fullname:['',Validators.required],
      email:['',[Validators.required,Validators.email]],
      password:['',[Validators.required,Validators.minLength(6)]],
      isAdmin:[false,Validators.nullValidator]
    });
  }



  ngOnInit(): void {

  }

  public register():void{
    const data=this.registerForm.value;
    if(!this.registerForm.valid)
    {
      window.alert("Invalid input");
      return;
    }
    // console.log(data);
    this.authService.register(data)
      .pipe(take(1),catchError(err => {window.alert(err.message); return of();}))
      .subscribe(response => {
        window.alert(response.message+" -> "+response.username)
        this.dialogRef.close();
      });
  }

}
