import {Component, EventEmitter, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../models/user.model";
import {MatDialogRef} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {AppComponent} from "../app.component";
import {of, Subscription} from "rxjs";
import {catchError, take} from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy{
  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }
  constructor(private authService:AuthService,public dialogRef:MatDialogRef<LoginComponent>,private router:Router) {

  }
  public message:string="";

  public login(data:{username:string,password:string}):void{
    this.authService.login(data)
      .pipe(take(1),catchError((err) => {
        console.log(err);
        this.message=err.error.message;
        return of(err)
      }))
      .subscribe(response => {
        if(response.hasOwnProperty('token')){
          localStorage.setItem('access-token', response.token);
          const user:User=response.user;
          this.authService.currentUser=user;
          localStorage.setItem('currentUser',JSON.stringify(user));
          this.dialogRef.close();
        }
      });
  }


}
