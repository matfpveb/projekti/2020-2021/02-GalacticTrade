export class Product {
    id: number;
    name: string;
    description: string | null;
    img: string | null;
    discount: number;
    CategoryId: number;
    price: number;
    count: number | undefined;
    deletable:boolean | undefined;
  }
