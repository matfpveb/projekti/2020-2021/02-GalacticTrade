import {Product} from "./product.model";

export class OrderItem {
  id:number;
  count:number;
  OrderId:number;
  Product:Product;
}
