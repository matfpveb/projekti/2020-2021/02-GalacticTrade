export class Order {
  id:number;
  firstName:string;
  lastName:string;
  email:string;
  address:string;
  city:string;
  paymentMethod:string;
  createdAt:string;
  total:number;
}
