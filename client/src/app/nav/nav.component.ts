import { RegisterComponent } from './../register/register.component';
import { CartComponent } from './../cart/cart.component';
import {Component, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {Observable, Subscription} from 'rxjs';
import {map, shareReplay, take} from 'rxjs/operators';
import {Group} from "../models/group.model";
import {GroupService} from "../services/group.service";
import { LoginComponent } from '../login/login.component';
import { MatDialog } from '@angular/material/dialog';
import {AuthService} from "../services/auth.service";
import {User} from "../models/user.model";
import {Router} from "@angular/router";
import { ProductService } from '../services/product.service';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {Product} from "../models/product.model";
import {MatTableDataSource} from "@angular/material/table";
import {ProductUpdateComponent} from "../product-update/product-update.component";
import {CategoryUpdateComponent} from "../category-update/category-update.component";
import {GroupUpdateComponent} from "../group-update/group-update.component";
import {Location} from "@angular/common";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit,OnChanges,OnDestroy {
  get groupService(): GroupService {
    return this._groupService;
  }



  get authService(): AuthService {
    return this._authService;
  }

  ngOnInit(): void {
    // this.subscribeAuth();
    this.cartProd();
  }

  cartItem:number = 0;
  cartProd(){
    if(localStorage.getItem('myData') != null){
      var cartCout = JSON.parse(localStorage.getItem('myData'));
      this.cartItem = cartCout.length;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  private activeSubs:Subscription[]=[];

  private unsubscribeAll(){
    this.activeSubs.forEach(sub=>{
      sub.unsubscribe();
    });
  }

  // private subscribeAuth(){
  //   const sub:Subscription=this.authService.authUser
  //     .subscribe((user)=>{
  //       this.authUser=user;
  //       this.isAuthenticated=true;
  //     },(err)=>{
  //       this.authUser=null;
  //       this.isAuthenticated=false;
  //     });
  //   this.activeSubs.push(sub);
  // }

  constructor(private breakpointObserver: BreakpointObserver, private _groupService:GroupService, public dialog: MatDialog, private _authService:AuthService, private router:Router, private productService: ProductService, private location:Location) {
    this._authService.myCartSub.subscribe((data)=>{
      this.cartItem = data;
    })
  }

  public logout(){
    this._authService.logout();
  }

  public displayLogin() {
    this.dialog.open(LoginComponent);
  }

  public displayRegister() {
    this.dialog.open(RegisterComponent);
  }

  public displayCart() {
    this.dialog.open(CartComponent);
  }

  public setSelectedCategory(categoryId: number, categoryName: string):void {
    this.productService.selectedCategoryId = categoryId;
    this.productService.selectedCategoryName = categoryName;
    this.productService.setAll();
  }

  ngOnDestroy(): void {
    this.unsubscribeAll();
  }

  public editCategoryForm(id:number):void{
    this.dialog.open(CategoryUpdateComponent,{data:{id}});
  }

  public editGroupForm(id:number):void{
    this.dialog.open(GroupUpdateComponent,{data:{id}});
  }

  public deleteGroup(id:number):void{
    this._groupService.deleteGroup(id)
      .pipe(take(1))
      .subscribe(response => {
        // window.alert(response.message);
        this.groupService.setAll();
      });
  }

  public deleteCategory(id:number):void{
    this._groupService.deleteCategory(id)
      .pipe(take(1))
      .subscribe(response => {
        // window.alert(response.message);
        this.groupService.setAll();
      });
  }

  public back():void{
    this.location.back();
  }

  public forward():void{
    this.location.forward();
  }

}
