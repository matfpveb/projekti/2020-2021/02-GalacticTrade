import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {OrderService} from "../services/order.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {Product} from "../models/product.model";
import {Order} from "../models/order.model";
import {Subscription} from "rxjs";
import {DateAdapter, MAT_DATE_LOCALE, NativeDateAdapter} from "@angular/material/core";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  providers:[
    {provide:MAT_DATE_LOCALE,useValue:'sr'},
  ]
})
export class OverviewComponent implements OnInit , OnDestroy{

  get orderService(): OrderService {
    return this._orderService;
  }

  public from: string;
  public to:string
  public search:string;
  constructor(private _orderService:OrderService,private changeDetectorRef: ChangeDetectorRef) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  public dataSource: MatTableDataSource<Order>;
  public dataSourceSub:Subscription=null;

  ngOnInit(): void {
    this.changeDetectorRef.detectChanges();
    this.orderService.paginator=this.paginator;
    this.refreshOrders();
    this.orderService.setTotalOrderValue();
    this.orderService.setTopProducts();
  }

  ngOnDestroy(): void {
    if (this.orderService.dataSource) {
      this.orderService.dataSource.disconnect();
    }
  }

  public stringToDate(date:string):Date{
    return new Date(date);
  }

  public refreshOrders():void{
    this.orderService.setAll();
  }

  onDateFromChange(event: string): void {
    this.from = event;
    this.orderService.startDate=new Date(this.from);
    this.refreshOrders();
  }
  onDateToChange(event: string): void {
    this.to = event;
    this.orderService.endDate=new Date(this.to);
    this.refreshOrders();
  }
  onSearchChange(newValue: string): void {
    this._orderService.searchText = newValue;
    this.refreshOrders();
  }
}
