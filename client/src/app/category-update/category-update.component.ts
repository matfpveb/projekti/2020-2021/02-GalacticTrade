import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GroupService} from "../services/group.service";
import {Observable} from "rxjs";
import {first} from "rxjs/operators";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-category-update',
  templateUrl: './category-update.component.html',
  styleUrls: ['./category-update.component.css']
})
export class CategoryUpdateComponent implements OnInit {
  get groupService(): GroupService {
    return this._groupService;
  }

  public editCategoryForm:FormGroup | null=null;

  constructor(private formBuilder:FormBuilder,
              private _groupService:GroupService,
              public dialogRef:MatDialogRef<CategoryUpdateComponent>,
              @Inject(MAT_DIALOG_DATA) public data:any) { }

  ngOnInit(): void {
    this.groupService.getCategoryById(this.data.id)
      .pipe(first())
      .subscribe(category => {
        this.editCategoryForm=this.formBuilder.group({
          name:[category.name,[Validators.required]],
          GroupId:[category.GroupId,[Validators.required]],
          description:[category.description ?? "",[Validators.nullValidator]]
        });
      });
  }

  public editCategoryFormSubmit():void{
    if(!this.editCategoryForm.valid)
    {
      window.alert("Invalid input");
      return;
    }

    const data=this.editCategoryForm.value;
    const id=this.data.id;

    const result:Observable<any>=this.groupService.updateCategory(id,data);
    result.pipe(first()).subscribe(object => {
      window.alert(object.message);
      this.groupService.setAll();
      this.dialogRef.close();
    });
  }

}
