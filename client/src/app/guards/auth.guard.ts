import { Injectable } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {CanActivate, Router} from "@angular/router";

@Injectable()
export class AuthGuard implements CanActivate{
  canActivate(): boolean {
    if(!this.authService.currentUser){
      this.router.navigate(['']);
      return false;
    }
    return true;
  }

  constructor(public authService:AuthService,public router:Router) { }
}
