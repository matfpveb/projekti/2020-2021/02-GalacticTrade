import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Group} from "../models/group.model";
import {AuthService} from "./auth.service";
import {Category} from "../models/category.model";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  get groups(): Observable<Group[]> {
    return this._groups;
  }

  private readonly getAllGroupsUrl:string=environment.allGroupsUrl;
  private readonly createCategoryUrl:string=environment.createCategoryUrl;
  private readonly createGroupUrl:string=environment.createGroupUrl;
  private readonly updateGroupUrl:string=environment.editGroupUrl;
  private readonly updateCategoryUrl:string=environment.editCategoryUrl;
  private readonly getCategoryByIdUrl:string=environment.categoryByIdUrl;
  private readonly getGroupByIdUrl:string=environment.groupByIdUrl;
  private readonly deleteGroupUrl:string=environment.deleteGroupUrl;
  private readonly deleteCategoryUrl:string=environment.deleteCategoryUrl;


  private _groups:Observable<Group[]>;

  public setAll():void{
    this._groups=this.http.get<Group[]>(this.getAllGroupsUrl);
  }

  public createGroup(data:{name:string,description:string|null}):Observable<any>{
    return this.http.post(this.createGroupUrl, data);
  }

  public updateGroup(id:number,data:{name:string,description:string|null}):Observable<any>{
    return this.http.put(this.updateGroupUrl+id.toString(), data);
  }

  public createCategory(data:{name:string,description:string|null,GroupId:number}):Observable<any>{
    return this.http.post(this.createCategoryUrl, data);
  }

  public updateCategory(id:number,data:{name:string,description:string|null,GroupId:number}):Observable<any>{
    return this.http.put(this.updateCategoryUrl+id.toString(), data);
  }

  public deleteGroup(id:number):Observable<any>{
    return this.http.delete(this.deleteGroupUrl+id.toString());
  }

  public deleteCategory(id:number):Observable<any>{
    return this.http.delete(this.deleteCategoryUrl+id.toString());
  }

  public getCategoryById(id:number):Observable<Category>{
    return this.http.get<Category>(this.getCategoryByIdUrl+id.toString());
  }

  public getGroupById(id:number):Observable<Group>{
    return this.http.get<Group>(this.getGroupByIdUrl+id.toString());
  }

  constructor(private http:HttpClient) {
    this.setAll();
  }
}
