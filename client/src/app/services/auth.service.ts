import {Injectable} from '@angular/core';
import {User} from "../models/user.model";
import {Observable, of, Subject} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError, first} from "rxjs/operators";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  set currentUser(value: User | null) {
    this._currentUser = value;
  }
  get currentUser(): User | null {
    return this._currentUser;
  }
  get authUser(): Observable<User> {
    return this.http.get<User>(this.authUserUrl);
  }

  constructor(private http:HttpClient) {
    const user:User | null | undefined=JSON.parse(localStorage.getItem('currentUser'));
    const token:string=localStorage.getItem('access-token');
    // console.log(user);
    // console.log(token);
    if (token && user)
    {
      this._currentUser=user;
    }
    else {
      this._currentUser=null;
    }
  }
  private readonly authUserUrl:string=environment.currentUserUrl;
  private readonly authLoginUrl:string=environment.loginUrl;
  private readonly registerUrl:string=environment.registerUrl;
  private _currentUser:User | null;

  myCartSub = new Subject<any>();



  public login(credentials:{username:string,password:string}):Observable<any>{
      return this.http.post(this.authLoginUrl, credentials);
  }

  public register(data):Observable<any>{
    return this.http.post(this.registerUrl,data);
  }

  public logout():void{
    localStorage.removeItem('access-token');
    localStorage.removeItem('currentUser');
    this._currentUser=null;
  }

}
