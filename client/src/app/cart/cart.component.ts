import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { validateBasis } from '@angular/flex-layout';
import { Product } from '../models/product.model';
import { environment } from '../../environments/environment'

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private auth: AuthService ) { }

  ngOnInit(): void {
    this.myCartInfo();
    this.updateMyCart();
  }

  public readonly environment=environment;

  getMyCartInfo: Product[] =[];
  myCartInfo(){
    if(localStorage.getItem('myData')) {
      this.getMyCartInfo = JSON.parse(localStorage.getItem('myData'));
      console.log(this.getMyCartInfo);
    }
  }

  addProductNum(id: number) {
    for(let num = 0; num < this.getMyCartInfo.length; num++){
      if(this.getMyCartInfo[num].id === id){

        this.getMyCartInfo[num].count += 1;
      }
    }
    localStorage.setItem('myData', JSON.stringify(this.getMyCartInfo));
    this.updateMyCart();
  }

  rmvProductNum(id: number) {
    for(let num = 0; num < this.getMyCartInfo.length; num++){
      if(this.getMyCartInfo[num].id === id){
        if(this.getMyCartInfo[num].count >= 1)
          this.getMyCartInfo[num].count -= 1;
        if(this.getMyCartInfo[num].count == 0) {
           this.removeProduct(this.getMyCartInfo[num].id);
        }
      }
    }
    localStorage.setItem('myData', JSON.stringify(this.getMyCartInfo));
    this.updateMyCart();
  }

  sum:number = 0;
  updateMyCart(){
    if(localStorage.getItem('myData')){
      this.getMyCartInfo = JSON.parse(localStorage.getItem('myData'));
      this.sum = this.getMyCartInfo.reduce(function(acc, val){
        return acc + ((val.price-(val.price*val.discount/100)) * val.count);
      }, 0)
    }
  }

  rmvAllProducts(){
    localStorage.removeItem('myData');
    this.getMyCartInfo = [];
    this.updateMyCart();
    this.cartNumber = 0;
    this.sum = 0;
    this.auth.myCartSub.next(this.cartNumber);
  }

  removeProduct(cartInfo){
    console.log(cartInfo);
    if(localStorage.getItem('myData')){
      this.getMyCartInfo = JSON.parse(localStorage.getItem('myData'));
      for(let num = 0; num < this.getMyCartInfo.length; num++){
        if(this.getMyCartInfo[num].id === cartInfo){
          this.getMyCartInfo.splice(num,1);
          localStorage.setItem('myData', JSON.stringify(this.getMyCartInfo));
          this.updateMyCart();
          this.cartNumUpdate();
        }
      }
    }
  }

  cartNumber:number = 0;
  cartNumUpdate(){
    var cartValue = JSON.parse(localStorage.getItem('myData'));
    this.cartNumber = cartValue.length;
    this.auth.myCartSub.next(this.cartNumber);
  }
}
